const pool = require('../database');

module.exports = function (req, res) {
  //res.header("Access-Control-Allow-Origin", "*");
    var where = "";
    if(req.params.search){
      where = "where name like '%"+req.params.search+"%' OR city like '%"+req.params.search+"%' OR country like '%"+req.params.search+"%' OR job like '%"+req.params.search+"%'";
    }
    console.log(req.params)
    var sqlquery = "select * from contacts " + where;
    pool.getConnection(function(err, connection) {
      if (err) { 
        console.log(err); 
        return; 
      }
      connection.query(sqlquery, function (err, result) {
          //if (err) throw err;
          connection.release();
          if (err){ 
            let data = {
              statusCode : 500,
              statusDesc : 'ERROR',
              data : err, 
            }
            res.send(JSON.stringify(data));
          }else{
            let data = {
              statusCode : 200,
              statusDesc : 'SUCCESS',
              data : result,
               
            }
            res.send(JSON.stringify(data));
            //8console.log(JSON.stringify(data));
          }
      });

    });
}