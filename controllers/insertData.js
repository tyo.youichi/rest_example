const pool = require('../database');

module.exports = function (req, res, next) {
	//res.header("Access-Control-Allow-Origin", "*");
	console.log('Got body:', req.body);

	var sqlquery = "INSERT INTO `vuedb`.`contacts` (`id`,`name`,`email`,`city`,`country`,`job`)VALUES(NULL,?,?,?,?,?);";
    var values = [
    		req.body.name,
    		req.body.email,
    		req.body.city,
    		req.body.country,
    		req.body.job
    	]
    pool.getConnection(function(err, connection) {
      if (err) { 
        console.log(err); 
        return; 
      }
      connection.query(sqlquery,values, function (err, result) {
          //if (err) throw err;
          connection.release();
          if (err){ 
          	let data = {
          		statusCode : 500,
          		statusDesc : 'ERROR',
          		data : err, 
          	}
          	res.send(JSON.stringify(data));
          }else{
          	let data = {
          		statusCode : 200,
          		statusDesc : 'SUCCESS',
          		data : result,
          		 
          	}
          	res.send(JSON.stringify(data));
          }
      });

    });
}