const mysql = require('mysql');
const pool = mysql.createPool({
  host: "yourhost",
  user: "youruser",
  password: "yourpass",
  database: "vuedb"
});

module.exports = pool;