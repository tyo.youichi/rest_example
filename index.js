const express = require('express');
const app = express();

//parser for method post and put
const body = require('body-parser');

//allow Cross Origin
const cors = require('cors');

//use body-parser
app.use(body.urlencoded({ extended: true }));
app.use(body.json());
app.use(body.raw());

//use cors
app.use(cors());

//config db vue/database/index.js
const pool = require('./database');

//controllers
const getData = require('./controllers/getData.js');
const insertData = require('./controllers/insertData');
const updateData = require('./controllers/updateData');
const searchData = require('./controllers/searchData');
const deleteData = require('./controllers/deleteData');

// route
app.get('/', getData)
app.get('/search/:search', searchData)
app.post('/add' , insertData)
app.put('/update', updateData)
app.delete('/delete/:id', deleteData)
// route

//running at port 3000
app.listen(3000)